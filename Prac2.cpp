//==============================================================================
// Copyright (C) John-Philip Taylor
// tyljoh010@myuct.ac.za
//
// This file is part of the EEE4084F Course
//
// This file is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//==============================================================================

#include "Prac2.h"
//------------------------------------------------------------------------------


// This is each thread's "main" function.  It receives a unique ID
void* Thread_Main(void* Parameter)
{
	int ID = *((int*)Parameter);
	unsigned char * window = new unsigned char[81];
	
	int y;
	for(int k = 0; k < window_sizes[ID]; k++)
	{
		y = thread_rows[ID * (Input.Height/Thread_Count) + k];
		for(int x = 0; x < Input.Width; x++)
		{
			for(int colour = 0; colour < Input.Components; colour++)
			{
				int i = 0;
				for(int fy = (y - 4); fy <= (y + 4); fy++)
				{
					for (int fx = (x - 4); fx <= (x + 4) ; fx++)
					{
						if ((fx >= 0) && (fx < Input.Width) && (fy >= 0) && (fy < Input.Height))
						{
							window[i] = Input.Rows[fy][fx * Input.Components + colour];
							i++;
						}
					}
				}
				std::sort(window, window + i);
				Output.Rows[y][x * Input.Components + colour] = window[i/2]/*sort(window, i)*/;
			}
		}
	}
	return 0;
}

void randomPartition()
{
	int i;
	int thread_window = Input.Height/Thread_Count;
	window_sizes = new int[Thread_Count];
	thread_rows = new int[Input.Height];
	i = 0;
	
	for (int j = 0; j<Thread_Count; j++)
	{
		if (j != Thread_Count-1) { window_sizes[j] = thread_window; }
		else { window_sizes[j] = Input.Height-i; }
		i += thread_window;
	}
	
	std::vector<int> rows;
	
	for (int j = 0; j<Input.Height; j++)
	{
		rows.push_back(j);
	}
	
	for (int j = 0; j<Thread_Count; j++)
	{
		srand (time(NULL));
		for (int k = 0; k<window_sizes[j]; k++)
		{
			int index = rand() % rows.size();
			thread_rows[j*thread_window+k] = rows[index];
			rows.erase(rows.begin()+index);
		}
	}
}
//------------------------------------------------------------------------------

int routine(char* image_name)
//int routine()
{
	//counter = 0;
	if(!Input.Read(image_name))
	{
		printf("Cannot read image\n");
		return -1;
	}

	// Allocated RAM for the output image
	if(!Output.Allocate(Input.Width, Input.Height, Input.Components)) return -2;

	
	// Spawn threads...
	int       Thread_ID[Thread_Count]; // Structure to keep the tread ID
	pthread_t Thread   [Thread_Count]; // pThreads structure for thread admin
	thread_data data[Thread_Count];
	randomPartition();
	//printf("done partitioning...\n");
	
	for(int j = 0; j < Thread_Count; j++)
	{
		Thread_ID[j] = j;
		pthread_create(Thread+j, NULL, Thread_Main, Thread_ID+j);
	}

	// Printing stuff is a critical section...
	pthread_mutex_lock(&Mutex);
	printf("Threads created :-)\n");
	pthread_mutex_unlock(&Mutex);

	tic();
	// Wait for threads to finish
	for(int j = 0; j < Thread_Count; j++)
	{
		if(pthread_join(Thread[j], 0))
		{
			pthread_mutex_lock(&Mutex);
			printf("Problem joining thread %d\n", j);
			pthread_mutex_unlock(&Mutex);
		}
	}

	// No more active threads, so no more critical sections required
	fprintf(fp, "%f", toc()/1e-3);
	
	// Write the output image
	if(!Output.Write("Data/Output.jpg"))
	{
		printf("Cannot write image\n");
		return -3;
	}
}

//------------------------------------------------------------------------------

int main(int argc, char** argv)
{
	int thread_counts[] = {2, 3, 4, 6, 8, 10, 12, 14, 20, 24, 28, 32, 34 , 46, 54, 64};
	char result_filename[50];
	char * images_name[] = {"Data/small.jpg", "Data/butterfly.JPG", "Data/fly.jpg", "Data/greatwall.jpg"};

	tic();
	pthread_mutex_init(&Mutex, 0);
	
	for (int i = 0; i < 1; i++) {
		sprintf(result_filename, "result_image%d_.csv", i);
		printf("%s\n", result_filename);
		fp = fopen(result_filename, "w+");
		
		if (fp == NULL) {
		  fprintf(stderr, "Can't open input file in.list!\n");
		  exit(1);
		}
		
		for (int k = 0; k < ARRAY_SIZE(thread_counts); k++) {
			
			fprintf(fp, "%d", thread_counts[k]);
			if (k != (ARRAY_SIZE(thread_counts) - 1)) fprintf(fp, ",");
		}
		fprintf(fp, "\n");

		for (int l = 0; l < Number_of_iterations; l++) {
			for (int j = 0; j < ARRAY_SIZE(thread_counts); j++) {
				Thread_Count = thread_counts[j];
				routine(images_name[i]);
					if (j != (ARRAY_SIZE(thread_counts) - 1)) fprintf(fp,",");
			}
			fprintf(fp, "\n");
		}
		fclose(fp);
	}

	// Clean-up
	pthread_mutex_destroy(&Mutex);
	return 0;
}
//------------------------------------------------------------------------------
