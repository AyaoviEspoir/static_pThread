//====================
//djsaya001
//====================

#include  "Prac2.h"
#include <iostream>
#include <algorithm>

unsigned char sort(unsigned char * array, int size)
{
	unsigned char k;
	for (int i = 0 ; i < size - 1; i++)
	{
		for (int j = 0;  j < size -1; j++)
		{
			if (array[j] > array[j+1])
			{
				k = array[j];
				array[j] = array[j+1];
				array[j+1] = k;
			}
		}
	}
	return array[size/2];
}

int main(int argc, char** argv)
{
	unsigned char * window = new unsigned char[81];
	// Read the input image
	if(!Input.Read("Data/chopper.jpeg"))
	{
		printf("Cannot read image\n");
		return -1;
	}
	
	// Allocated RAM for the output image
	if(!Output.Allocate(Input.Width, Input.Height, Input.Components)) return -2;
	
	tic();
	
	/*
	allocate outputPixelValue[image width][image height]
	allocate window[window width * window height]
	edgex := (window width / 2) rounded down
	edgey := (window height / 2) rounded down
	for x from edgex to image width - edgex
		for y from edgey to image height - edgey
		   i = 0
		   for fx from 0 to window width
			   for fy from 0 to window height
				   window[i] := inputPixelValue[x + fx - edgex][y + fy - edgey]
				   i := i + 1
		   sort entries in window[]
		   outputPixelValue[x][y] := window[window width * window height / 2]
	 */
	for(int y = 0; y < Input.Height; y++)
	{
		//for(int x = 0; x < Input.Width*Input.Components; x++)
		for(int x = 0; x < Input.Width; x++)
		{
			for(int colour = 0; colour < Input.Components; colour++)
			{
				int i = 0;
				for(int fy = y - 4 ; fy <= (y + 4); fy++)
				{
					for (int fx = (x - 4); fx <= (x + 4) ; fx++)
					{
						if ((fx >= 0) && (fx < Input.Width) && (fy >= 0) && (fy < Input.Height))
						{
							window[i] = Input.Rows[fy][fx * Input.Components + colour];
							i++;
						}
					}
				}
				//std::sort(window, window + i);
				Output.Rows[y][x * Input.Components + colour] = /*window[i/2]*/sort(window, i);
			}
		}
	}
	printf("Time = %lg ms\n", (double)toc()/1e-3);
	
	// Write the output image
	if(!Output.Write("Data/Output.jpg"))
	{
		printf("Cannot write image\n");
		return -3;
	}
	
	return 0;
}
