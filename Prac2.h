//==============================================================================
// Copyright (C) John-Philip Taylor
// tyljoh010@myuct.ac.za
//
// This file is part of the EEE4084F Course
//
// This file is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//==============================================================================

#ifndef Prac2_h
#define Prac2_h
//------------------------------------------------------------------------------

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <algorithm>
#include <vector>
//------------------------------------------------------------------------------

#include "JPEG.h"
#include "Timer.h"
//------------------------------------------------------------------------------

#define Filter_size 9
#define Middle (Filter_size*Filter_size)/2
#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))
#define Number_of_iterations 5
//------------------------------------------------------------------------------

// Input and output buffers
JPEG Input;
JPEG Output;
//------------------------------------------------------------------------------

pthread_mutex_t Mutex; // General-purpose MutEx
//------------------------------------------------------------------------------

// The thread "main" function.  Receives a unique thread ID as parameter
void* Thread_Main(void* Parameter);
int routine(char* image_name);

/* struct to hold data to be passed to a thread
   this shows how multiple data items can be passed to a thread */
struct thread_data
{
    int thread_id;
    int start;
    int end;
};

typedef thread_data * Thread_data;
int * thread_rows;
int * window_sizes;
int Thread_Count;
FILE * fp;
//------------------------------------------------------------------------------

#endif
//------------------------------------------------------------------------------
